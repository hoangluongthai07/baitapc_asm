﻿using System;

namespace EX_ChuViDienTich
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine("Tính Diện Tích và Chu Vi của hình chữ nhật");

            Console.WriteLine("Nhập chiều dài hình chữ nhật");
            double chieuDai = double.Parse(Console.ReadLine());

            Console.WriteLine("Nhập chiều rộng hình chữ nhật");
            double chieuRong = double.Parse(Console.ReadLine());

            double ChuViHCN = (chieuDai + chieuRong) * 2;
            double DTHCN = chieuDai * chieuRong;

            Console.WriteLine("Chu vi hình chữ nhật là: " + ChuViHCN);

            Console.WriteLine("Diện tích hình chữ nhật là: " + DTHCN);
        }
    }
}
